IoT Blink
=========
Rewrite blink example in Intel IoT XDK with ping. 

Author
------
* hadrihl (hadrihilmi@gmail.com)

Tools
-----
* Node.js
* MRAA library
* net-ping
* Intel IoT XDK

Guides to run
--------------
1. Connect to IoT device 
2. Build 
4. Run