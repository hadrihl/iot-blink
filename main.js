/* Make with love in Penang :) */

var mraa = require("mraa");
var ping = require("net-ping");

console.log("MRAA version: " + mraa.getVersion());

var session = ping.createSession();
var led = new mraa.Gpio(13);
led.dir(mraa.DIR_OUT);
var ledState = true;

main();

function main() {
    /* ping MIMOS Berhad for network conn testing */
    session.pingHost("202.45.139.67", function(error, target) {
        if(error) dead(); else alive();
    });
    
    setTimeout(main, 1000);
}

function alive() {
    led.write(1); led.write(0);
}

function dead() {
    led.write(1);
}